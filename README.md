Ein Projekt von Tobias Behn, Jonas Christmann, Johanna Meyer, André Schuldt und Bennedict Schweimer.
Entstanden im Rahmen des Moduls "Mixed Reality" an der HAW-Hamburg.

![Paper des Projekts](Assets/Paper/UniversumPaper.PNG)

## Aufzeichnung:

[![](http://img.youtube.com/vi/YhUfpzfDjMU/0.jpg)](https://www.youtube.com/watch?v=YhUfpzfDjMU "SolarSystemVR")


